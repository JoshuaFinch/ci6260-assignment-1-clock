/*
 * The shader.cpp and shader.hpp are not my own, belonging to http://www.opengl-tutorial.org/ and are
 * simply used for loading my shaders
 * 
 * The code in the following files is my own:
 *	- Clock.cpp
 *	- SimpleFragmentShader.fragmentshader
 *	- SimpleVertexShader.vertexshader
*/

#ifndef SHADER_HPP
#define SHADER_HPP

GLuint LoadShaders(const char * vertex_file_path,const char * fragment_file_path);

#endif
