/* 
* Computer Graphics - Clock - Assignment 1
* Author: Joshua Finch, K1011450
* 
* The following code is all my own work unless otherwise mentioned.
* I learnt how to create the C++/OpenGL window through the tutorials at the following sources:
* http://www.opengl-tutorial.org/
* http://arcsynthesis.org/gltut
* http://open.gl/ 
* 
* This code requires version 3.3 or above of OpenGL running on your graphics card to run.
* 
* This code uses the GLEW, GLFW and GLM libraries. These are not my own work and are simply used
* to make creating an OpenGL window easier, and managing OpenGL extensions. GLM library is used to
* simplify commonly used basic maths functions.
* 
* The shader.cpp and shader.hpp are not my own, belonging to http://www.opengl-tutorial.org/ and are
* simply used for loading my shaders
* 
* The code in the following files is my own:
*	- Clock.cpp
*	- SimpleFragmentShader.fragmentshader
*	- SimpleVertexShader.vertexshader
*
*/

// Include standard headers
#include <stdio.h>
#include <stdlib.h>

#include <iostream>
#include <vector>
#include <time.h>

#define _USE_MATH_DEFINES // For getting M_PI
#include <math.h>

// Include GLEW to handle OpenGL extensionss
#include <GL/glew.h>

// Include GLFW to handle the window/keyboard
#include <GL/glfw.h>

// Include GLM maths library
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
using namespace glm;

// Include Shaders
#include "shader.hpp"

float fMinuteLine[12];
float fHand[12];
float fCirclePoints[1080];
float fPinPoints[1080];
float fPinPoints2[1080];
float fPinColour[1080];
float fPinColour2[1080];
float fLollipop[1080];
float fLollipopColour[1080];

GLuint colorbuffer5;
GLuint colorbuffer4;
GLuint colorbuffer3;
GLuint colorbuffer2;
GLuint colorbuffer;

float fMinuteHandColour[12] = {
	0.0f, 0.0f, 0.0f,
	0.0f, 0.0f, 0.0f,
	0.0f, 0.0f, 0.0f,
	0.0f, 0.0f, 0.0f
};

float fSecondHandColour[12] = {
	1.0f, 0.0f, 0.0f,
	1.0f, 0.0f, 0.0f,
	1.0f, 0.0f, 0.0f,
	1.0f, 0.0f, 0.0f
};

const int countVBO = 67; 
// 60 for minute/second notches, 
// 1 for hour hand, 1 for minute hand, 1 for seconds hand,
// 1 for circle, 1 for pin point, 1 for smaller pin point,
// 1 for lollipop

GLuint VertexArrayID;
GLuint uiVBO[countVBO];

glm::mat4 MVP;

// ADDED INTO REPORT
void createPin() {
	float radius = 0.02f;
	float radius2 = 0.005f;

	for(int i = 0; i < 360; i++) {
		fPinPoints[(i*3)] = glm::cos((float)i) * radius;
		fPinPoints[(i*3)+1] = glm::sin((float)i) * radius;
		fPinPoints[(i*3)+2] = 0.0f;

		fPinColour[(i*3)] = 1.0f;
		fPinColour[(i*3)+1] = 0.0f;
		fPinColour[(i*3)+2] = 0.0f;

		fPinPoints2[(i*3)] = glm::cos((float)i) * radius2;
		fPinPoints2[(i*3)+1] = glm::sin((float)i) * radius2;
		fPinPoints2[(i*3)+2] = 0.0f;

		fPinColour2[(i*3)] = 0.55f;
		fPinColour2[(i*3)+1] = 0.55f;
		fPinColour2[(i*3)+2] = 0.55f;

		//Setup lollipop colour here, once as opposed to every frame in createSecondsHandCircle()
		fLollipopColour[(i*3)] = 1.0f;
		fLollipopColour[(i*3)+1] = 0.0f;
		fLollipopColour[(i*3)+2] = 0.0f;
	}
}

// ADDED INTO REPORT
void createSecondsHandCircle(float angle) {

	float radius = 0.04f;

	// Start pointing at zero hours (12th Hour)
	mat4 initialOffsetRotationMatrix = rotate(90.0f, 0.0f, 0.0f, 1.0f); 

	// Minus angle to go ClockWise as oppose to CounterClockWise
	mat4 rotationMatrix = rotate(-angle, 0.0f, 0.0f, 1.0f); 

	mat4 comboMatrix = rotationMatrix * initialOffsetRotationMatrix;

	for(int i = 0; i < 360; i++) {
		vec4 point(
			glm::cos((float)i) * radius + 0.55f, 
			glm::sin((float)i) * radius, 
			0.0f, 
			1.0f);

		vec4 pointt = comboMatrix * point;

		fLollipop[(i*3)] = pointt.x;
		fLollipop[(i*3)+1] = pointt.y;
		fLollipop[(i*3)+2] = 0.0f;

		// Moved to createPin() so that the colour setting only occurs on startup as oppsoed to every frame
		//fLollipopColour[(i*3)] = 1.0f;
		//fLollipopColour[(i*3)+1] = 0.0f;
		//fLollipopColour[(i*3)+2] = 0.0f;
	}
}

// ADDED INTO REPORT
void createCircle(float radius) {
	for(int i = 0; i < 360; i++) {
		fCirclePoints[(i*3)] = glm::cos((float)i) * radius;
		fCirclePoints[(i*3)+1] = glm::sin((float)i) * radius;
		fCirclePoints[(i*3)+2] = 0.0f;
	}
}

// ADDED INTO REPORT
void createLine(int index)
{
	float clockRadius = 0.75f;
	float start = 0.0f;
	float end, angle;

	if(index % 5 == 0) {
		angle = (float) (index*30);
		start = 0.0f;
		end = 0.8f;
	} else {
		angle = (float) (index*6);
		start = 0.0f;
		end = 0.3f;
	}

	vec4 p1(start, start - end, 0.0f, 1.0f);
	vec4 p2(end, start - end, 0.0f, 1.0f);
	vec4 p3(start, end, 0.0f, 1.0f);
	vec4 p4(end, end, 0.0f, 1.0f);

	mat4 scaleMatrix = scale(0.1f, 0.015f, 1.0f);
	mat4 translationMatrixOffset = translate(-clockRadius, 0.0f, 0.0f);
	mat4 rotationMatrix = rotate(angle, 0.0f, 0.0f, 1.0f);
	mat4 rotatedLine = rotationMatrix * translationMatrixOffset * scaleMatrix;
	mat4 translationMatrixRadius = translate(clockRadius, 0.0f, 0.0f);
	mat4 moveToEdgeMatrix = rotatedLine * translationMatrixRadius;

	vec4 p1t = moveToEdgeMatrix * p1;
	vec4 p2t = moveToEdgeMatrix * p2;
	vec4 p3t = moveToEdgeMatrix * p3;
	vec4 p4t = moveToEdgeMatrix * p4;

	fMinuteLine[0] = p1t.x;	fMinuteLine[1] = p1t.y; fMinuteLine[2] = p1t.z; // Point 1 (x,y,z)
	fMinuteLine[3] = p2t.x; fMinuteLine[4] = p2t.y; fMinuteLine[5] = p2t.z; // Point 2 (x,y,z)
	fMinuteLine[6] = p3t.x; fMinuteLine[7] = p3t.y; fMinuteLine[8] = p3t.z; // Point 3 (x,y,z)
	fMinuteLine[9] = p4t.x; fMinuteLine[10] = p4t.y; fMinuteLine[11] = p4t.z; // Point 4 (x,y,z)
}

// ADDED INTO REPORT
void createHand(float angle, float origin, float xLength, float yMaxLength, float yMinLength) {

	vec4 p1(origin,	0 - yMaxLength,	0.0f, 1.0f);
	vec4 p2(xLength,	0 - yMinLength,	0.0f, 1.0f);
	vec4 p3(origin,	yMaxLength,		0.0f, 1.0f);
	vec4 p4(xLength,	yMinLength,		0.0f, 1.0f);

	mat4 initialOffsetRotationMatrix = rotate(90.0f, 0.0f, 0.0f, 1.0f); // Start pointing at zero hours (12th Hour)
	mat4 rotationMatrix = rotate(-angle, 0.0f, 0.0f, 1.0f); // Minus angle to go ClockWise as oppose to CounterClockWise
	mat4 comboMatrix = rotationMatrix * initialOffsetRotationMatrix;

	vec4 p1t = comboMatrix * p1;
	vec4 p2t = comboMatrix * p2;
	vec4 p3t = comboMatrix * p3;
	vec4 p4t = comboMatrix * p4;

	fHand[0] = p1t.x; fHand[1] = p1t.y; fHand[2] = p1t.z; // Point 1 (x,y,z)
	fHand[3] = p2t.x; fHand[4] = p2t.y; fHand[5] = p2t.z; // Point 2 (x,y,z)
	fHand[6] = p3t.x; fHand[7] = p3t.y; fHand[8] = p3t.z; // Point 3 (x,y,z)
	fHand[9] = p4t.x; fHand[10] = p4t.y; fHand[11] = p4t.z; // Point 4 (x,y,z)
}

// ADDED INTO REPORT
// Called every update from updateScene() with current time in seconds
void createClock(int total_seconds)
{
	int hours = (total_seconds / 60 / 60) % 24; // Between 0 and 23 hours (total 24)
	int minutes = (total_seconds / 60) % 60; // Between 0 and 59 minutes (total 60)
	int seconds = total_seconds % 60; // Between 0 and 59 seconds (total 60)
	int minutesPassed = hours * 60 + minutes; // Current minute of the day between 0 and 1440 (60m * 24h)
	int degreesPerMinute = 360/60; // Degrees in a circle / total minutes on a clock = 6

	// Set transforms for the hour hand
	createHand(minutesPassed / 12 * degreesPerMinute, -0.15f, 0.5f, 0.04f, 0.035f);
	glBindBuffer(GL_ARRAY_BUFFER, uiVBO[60]);
	glBufferData(GL_ARRAY_BUFFER, 12*sizeof(float), fHand, GL_STREAM_DRAW);

	// Set transforms for the minute hand
	createHand(minutes * degreesPerMinute, -0.15f, 0.6f, 0.035f, 0.03f);
	glBindBuffer(GL_ARRAY_BUFFER, uiVBO[61]);
	glBufferData(GL_ARRAY_BUFFER, 12*sizeof(float), fHand, GL_STREAM_DRAW);

	// Set transforms for the seconds hand
	createHand(seconds * degreesPerMinute, -0.25f, 0.55f, 0.005f, 0.005f);
	glBindBuffer(GL_ARRAY_BUFFER, uiVBO[62]);
	glBufferData(GL_ARRAY_BUFFER, 12*sizeof(float), fHand, GL_STREAM_DRAW);

	// Set transforms for the seconds hand end circle
	createSecondsHandCircle(seconds * degreesPerMinute);
	glBindBuffer(GL_ARRAY_BUFFER, uiVBO[66]);
	glBufferData(GL_ARRAY_BUFFER, 1080*sizeof(float), fLollipop, GL_STREAM_DRAW);
}


int setupWindow() {
	// Initialise GLFW
	if( !glfwInit() )
	{
		fprintf( stderr, "Failed to initialize GLFW\n" );
		return EXIT_FAILURE;
	}

	glfwOpenWindowHint(GLFW_FSAA_SAMPLES, 4);
	glfwOpenWindowHint(GLFW_OPENGL_VERSION_MAJOR, 3);
	glfwOpenWindowHint(GLFW_OPENGL_VERSION_MINOR, 3);
	glfwOpenWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	// Open a window and create its OpenGL context
	if( !glfwOpenWindow( 1024, 1024, 0,0,0,0, 32,0, GLFW_WINDOW ) )
	{
		fprintf( stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Upgrade your grpahics card and / or graphics card drivers.\n" );
		glfwTerminate();
		return EXIT_FAILURE;
	}

	// Initialize GLEW
	glewExperimental = true; // Needed for core profile
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		return EXIT_FAILURE;
	}

	glfwSetWindowTitle( "Joshua Finch - K1011450 - Computer Graphics Assignment - Clock" );

	// Ensure we can capture the escape key being pressed below
	glfwEnable( GLFW_STICKY_KEYS );

	return EXIT_SUCCESS;
}

// ADDED INTO REPORT
void bindColourBuffers() {
	// Generate and bind the red rectangle colour buffer
	// Used for the second hand
	glGenBuffers(1, &colorbuffer);
	glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(fSecondHandColour), fSecondHandColour, GL_STATIC_DRAW);

	// Generate and bind the black rectangle colour buffer
	// Used for minute hour, hour hand, and notches 
	glGenBuffers(1, &colorbuffer2);
	glBindBuffer(GL_ARRAY_BUFFER, colorbuffer2);
	glBufferData(GL_ARRAY_BUFFER, sizeof(fMinuteHandColour), fMinuteHandColour, GL_STATIC_DRAW);

	// Generate and bind the red, outer pin colour
	glGenBuffers(1, &colorbuffer3);
	glBindBuffer(GL_ARRAY_BUFFER, colorbuffer3);
	glBufferData(GL_ARRAY_BUFFER, sizeof(fPinColour), fPinColour, GL_STATIC_DRAW);

	// Generate and bind the 55% grey, inner pin colour
	glGenBuffers(1, &colorbuffer4);
	glBindBuffer(GL_ARRAY_BUFFER, colorbuffer4);
	glBufferData(GL_ARRAY_BUFFER, sizeof(fPinColour2), fPinColour2, GL_STATIC_DRAW);

	// Generate and Bind the red, second hand circle colour
	glGenBuffers(1, &colorbuffer5);
	glBindBuffer(GL_ARRAY_BUFFER, colorbuffer5);
	glBufferData(GL_ARRAY_BUFFER, sizeof(fLollipopColour), fLollipopColour, GL_STATIC_DRAW);
}

// ADDED INTO REPORT
void setupScene() {
	// Create Vertex Array Object
	glGenVertexArrays(1, &VertexArrayID);
	glBindVertexArray(VertexArrayID);

	// Generate all of the required vertex buffer objects
	glGenBuffers(countVBO, uiVBO);
 
	// Bind the 60 notches to their VBOs
	for(int i = 0; i < 60; i++) {
		createLine(i);
		glBindBuffer(GL_ARRAY_BUFFER, uiVBO[i]); // Minute/Second Notch
		glBufferData(GL_ARRAY_BUFFER, 12*sizeof(float), fMinuteLine, GL_STATIC_DRAW);
	}

	// Clock face circle, bound to its VBO
	createCircle(0.8f);
	glBindBuffer(GL_ARRAY_BUFFER, uiVBO[63]);
	glBufferData(GL_ARRAY_BUFFER, 1080*sizeof(float), fCirclePoints, GL_STATIC_DRAW);

	// Setup the two pin circle points, colours
	// and the second hand circle colours
	createPin();
	
	// Bind the outer pin to its VBO
	glBindBuffer(GL_ARRAY_BUFFER, uiVBO[64]);
	glBufferData(GL_ARRAY_BUFFER, 1080*sizeof(float), fPinPoints, GL_STATIC_DRAW);

	// Bind the inner pin to its VBO
	glBindBuffer(GL_ARRAY_BUFFER, uiVBO[65]); // Smaller Pin Point
	glBufferData(GL_ARRAY_BUFFER, 1080*sizeof(float), fPinPoints2, GL_STATIC_DRAW);

	bindColourBuffers();
}

// ADDED INTO REPORT
void setupMVP() {
	glm::mat4 Projection = glm::ortho(-1.0f,1.0f,-1.0f,1.0f,0.0f,100.0f); // In world coordinates

	glm::mat4 View = glm::lookAt(
		glm::vec3(0,0,5), // Camera position in World Space
		glm::vec3(0,0,0), // Look at Origin
		glm::vec3(0,1,0)  // Head is Up
		);

	glm::mat4 Model = glm::mat4(1.0f);

	MVP = Projection * View * Model;
}

// ADDED INTO REPORT
// Called Every Update within the Main Function
void updateScene() {
	createClock((time_t)time(NULL));
}

// ADDED INTO REPORT
void renderScene() {
	glEnableVertexAttribArray(0); // Verticies
	glEnableVertexAttribArray(1); // Colour

	// Bind the colour buffers and vertex buffers
	// for each object here...

	// Start painting with the four vertex black colour buffer
	glBindBuffer(GL_ARRAY_BUFFER, colorbuffer2);
	glVertexAttribPointer(1,3,GL_FLOAT,GL_FALSE,0,(void*)0);

	// Circle Render
	glBindBuffer(GL_ARRAY_BUFFER, uiVBO[63]);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	glDrawArrays(GL_LINE_STRIP, 0, 360);

	// Minute/Second Notch Render
	for(int i = 0; i < 60; i++) {
		glBindBuffer(GL_ARRAY_BUFFER, uiVBO[i]); // Minute/Second Notch VBO
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
		glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	}

	// Hour Hand Render
	glBindBuffer(GL_ARRAY_BUFFER, uiVBO[60]); // Hour Hand VBO
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

	// Minute Hand Render
	glBindBuffer(GL_ARRAY_BUFFER, uiVBO[61]); // Minute Hand VBO
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

	// Start painting with the four vertex red colour buffer
	glBindBuffer(GL_ARRAY_BUFFER, colorbuffer);
	glVertexAttribPointer(1,3,GL_FLOAT,GL_FALSE,0,(void*)0);

	// Second Hand Render
	glBindBuffer(GL_ARRAY_BUFFER, uiVBO[62]); // Seconds Hand VBO
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

	// Start painting with the 360 vertex red colour buffer
	glBindBuffer(GL_ARRAY_BUFFER, colorbuffer5);
	glVertexAttribPointer(1,3,GL_FLOAT,GL_FALSE,0,(void*)0);

	// Second Hand Circle Render
	glBindBuffer(GL_ARRAY_BUFFER, uiVBO[66]); // Seconds Hand VBO
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 360);

	// Setup 360-Vertex Colour (larger point)
	glBindBuffer(GL_ARRAY_BUFFER, colorbuffer3);
	glVertexAttribPointer(1,3,GL_FLOAT,GL_FALSE,0,(void*)0);

	// Pin Point Render
	glBindBuffer(GL_ARRAY_BUFFER, uiVBO[64]); // Seconds Hand VBO
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 360);

	// Setup 360-Vertex Colour (smaller point)
	glBindBuffer(GL_ARRAY_BUFFER, colorbuffer4);
	glVertexAttribPointer(1,3,GL_FLOAT,GL_FALSE,0,(void*)0);

	// Smaller Pin Point Render
	glBindBuffer(GL_ARRAY_BUFFER, uiVBO[65]); // Seconds Hand VBO
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	glDrawArrays(GL_TRIANGLE_FAN, 0, 360);

	glDisableVertexAttribArray(0); // Verticies
	glDisableVertexAttribArray(1); // Colour
}

int main( void )
{
	if(setupWindow() == EXIT_FAILURE)
		return EXIT_FAILURE;

	// Create and compile our GLSL program from the shaders
	GLuint programID = LoadShaders( "SimpleVertexShader.vertexshader", "SimpleFragmentShader.fragmentshader");
	GLuint MatrixID = glGetUniformLocation(programID, "MVP");

	setupMVP();

	glClearColor(.95f, .95f, .95f, 0.0f); // 95% Grey

	setupScene();

	do{
		glClear( GL_COLOR_BUFFER_BIT );

		// Use the shader
		glUseProgram(programID);

		// Send our transformation to the currently bound shader, 
		// in the "MVP" uniform
		glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);

		updateScene(); // Move clock hands
		renderScene(); // Render clock

		// Swap buffers
		glfwSwapBuffers();

	} while( glfwGetKey( GLFW_KEY_ESC ) != GLFW_PRESS &&
		glfwGetWindowParam( GLFW_OPENED ) );

	// Close OpenGL window and terminate GLFW
	glfwTerminate();

	// Cleanup VBO
	glDeleteBuffers(countVBO, uiVBO);
	glDeleteProgram(programID);
	glDeleteVertexArrays(1, &VertexArrayID);

	return 0;
}